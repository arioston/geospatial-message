FROM anapsix/alpine-java:8u162b12_server-jre_unlimited

MAINTAINER ariostonj@hotmail.com

COPY /scripts/wait-for-it.sh /geospatial/scripts/

ADD build/libs/geospatial-messenger-0.0.1-SNAPSHOT.jar /geospatial/app.jar

EXPOSE 3000

RUN ["chmod", "+x", "/geospatial/scripts/wait-for-it.sh"]
#CMD ["./geospatial/scripts/wait-for-it.sh", "db:5432", "java", "-jar", "/geospatial/app.jar"]
#CMD ["java", "-jar", "/geospatial/app.jar"]