package br.com.grokar.web

import br.com.grokar.domains.Message
import br.com.grokar.repository.MessageRepository
import org.postgis.PGbox2d
import org.postgis.Point
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/message")
class MessageController(val repository: MessageRepository) {
    val broadcaster = ReactiveBroadcaster()

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun create(@RequestBody message: Message) : Message {
        val msg = repository.insert(message)
        broadcaster.send(msg)
        return msg
    }

    @PutMapping
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun updateContent(@RequestBody message: Message) : Message {
        val msg = repository.updateContent(message.id, message.content)
        broadcaster.send(msg)
        return msg
    }

    @GetMapping
    fun list() = repository.findAll()

    @GetMapping("/subscribe")
    fun subscribe() = broadcaster.subscriber()
}