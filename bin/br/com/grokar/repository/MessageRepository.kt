package br.com.grokar.repository

import br.com.grokar.db.Messages
import br.com.grokar.domains.Message
import br.com.grokar.utils.getMessage
import br.com.grokar.utils.insertQuery
import br.com.grokar.utils.within
import org.jetbrains.exposed.sql.*
import org.postgis.PGbox2d
import org.postgis.Point
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Transactional

/**
 * @inheritDoc
 * */
interface MessageRepository: CrudRepository<Message, Int>

@Repository
@Transactional
class MessageRepositoryImpl: MessageRepository {

    override fun updateLocation(id: Int, location: Point) {
        location.srid = 4326
        Messages.update({ Messages.id eq id }) { it[Messages.location] = location }
    }

    override fun deleteAll() = Messages.deleteAll()

    override fun findAll() = Messages.selectAll().map{ it.getMessage() }

    override fun insert(t: Message): Message {
        t.id = Messages.insert(insertQuery(t))[Messages.id]
        return t
    }

    override fun createTable() = SchemaUtils.create(Messages)

    override fun updateContent(id: Int, content: String) {
        Messages.update({ Messages.id eq id }) { it[Messages.content] = content }
    }
}