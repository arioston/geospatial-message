package br.com.grokar.repository

import br.com.grokar.domains.Message
import org.postgis.PGbox2d
import org.postgis.Point

interface CrudRepository<T, K> {

    fun createTable()
    fun insert(t: T): T
    fun findAll(): Iterable<T>
    fun deleteAll(): Int
    fun updateLocation(id: Int, location: Point)
    fun updateContent(id: Int, content: String) : T
}