package br.com.grokar.domains

import org.postgis.Point

class Message (
    var content: String,
    var location: Point? = null,
    var id: Int
)