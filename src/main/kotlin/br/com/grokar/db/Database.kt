package br.com.grokar.db

import br.com.grokar.utils.point
import org.jetbrains.exposed.sql.Table

object Messages : Table() {
    val id = integer("id").autoIncrement().primaryKey()
    val content = text("content")
    val location = point("location").nullable()
}