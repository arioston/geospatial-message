import React from 'react';
import ReactDOM, { render } from 'react-dom';
import axios from 'axios';
import { _ } from 'lodash';
import {
  CircleMarker,
  Map,
  Popup,
  TileLayer,
} from 'react-leaflet';

const ESCAPE_KEY = 27, 
      ENTER_KEY = 13;

const alreadyExists = (state, coordinates) => state.points.find(point => point.coordinates == coordinates)

class Feature extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editText: this.props.content
    }
    this.bindRef = this.bindRef.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.close = this.close.bind(this);
  }
  componentDidUpdate(prevProps) {
    if (!prevProps.editing && this.props.editing && this.refs.editField) {
      var node = ReactDOM.findDOMNode(this.refs.editField);
      node.focus();
      node.setSelectionRange(node.value.length, node.value.length);
    }
  }
  handleEdit() {
    this.props.onEdit();
    this.setState({editText: this.props.content});
  }
  bindRef(item) {
    if(item) {
      item.ondblclick = this.handleEdit;
    }
  }
  handleSubmit(event) {
    var val = this.state.editText.trim();
    if(val) {
      this.props.onSave(val);
      this.setState({editText: val});
    }
  }
  handleChange(event) {
    if (this.props.editing) {
      this.setState({editText: event.target.value});
    }
  }
  handleKeyDown(event) {
    if (event.which === ESCAPE_KEY) {
      this.setState({editText: this.props.content});
      this.props.onCancel(event);
    } else if (event.which === ENTER_KEY) {
      this.handleSubmit(event);
    }
  }
  close(){
    handleKeyDown({which: ESCAPE_KEY})
  }
  render() {
    const {
      location :{ coordinates }, index, content, editing
    } = this.props;
    return (
      <div style={{ display: 'none' }}>
        <CircleMarker center={coordinates} color="red" radius={10} key={`cirlce-${coordinates[0]}-${coordinates[1]}`}>
          <Popup closeOnClick={this.close}>
            <span ref={this.bindRef}>
              {editing ? 
                <input
                  ref="editField"
                  value={this.state.editText}
                  onBlur={this.handleSubmit}
                  onChange={this.handleChange}
                  onKeyDown={this.handleKeyDown}
                /> : content}</span>
          </Popup>
        </CircleMarker>
      </div>
    )
  }
}

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      center: [51.505, -0.09],
      zoom: 13,
      points: [],
    }
    this.edit = this.edit.bind(this);
    this.cancel = this.cancel.bind(this);
    this.save = this.save.bind(this);
    this.addFeature = this.addFeature.bind(this);
    this.eventListener = this.eventListener.bind(this);
    this.updatePoint = this.updatePoint.bind(this);
  }
  componentWillMount() {
    var source = new EventSource("/message/subscribe");
    source.addEventListener('message', this.eventListener);

    axios
      .get("/message")
      .then(response => this.setState({points : [...response.data]}))
      .catch(err => console.log("Erro ao recuperar a lista", err));
  }
  eventListener(event) {
    var message = JSON.parse(event.data);
    const { coordinates } = message.location;
    if(alreadyExists(this.state, coordinates)) {
      this.updatePoint(message);
    } else {
      this.setState({
        points: [...this.state.points,  message]
      });
    }
  }
  updatePoint(newPoint) {
    const { points } = this.state;
    const index = _.findIndex(points, ["id", newPoint.id])
    const updatePoint = [...points];
    updatePoint[index].content = text;
    this.setState({
      points: [...updatePoint]
    });
  }
  save(index, text) {
    const { points } = this.state;
    const updatePoint = [...points];
    updatePoint[index].content = text;

    axios
      .put("/message", updatePoint[index])
      .then(response => console.log("Marker update"))
      .catch(err => console.log("Error ao realizar update"))
  }
  edit(point) {
    this.setState({
      editing: point
    })
  }
  cancel() {
    this.setState({
      editing: null
    })
  }
  addFeature({latlng: { lat, lng }}) {
    if(!alreadyExists(this.state, [lat, lng])) {
      axios
        .post("/message", {
          content: "default text",
          location: {
            type: "Point",
            coordinates: [lat, lng]
          }
        })
        .then(response => console.log("sucesso na requisição", response))
        .catch(err => console.log(err));
    }
  }
  render() {
    const {
      center,
      zoom,
      points,
    } = this.state;
    return (
      <div className="leaflet-container">
        <Map
          center={center}
          zoom={zoom}
          ondblclick={this.addFeature}
        >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url='http://{s}.tile.osm.org/{z}/{x}/{y}.png'
          />
          {
            points.map((point, index) => 
              <Feature 
                key={`feature-${index}`}
                {...point}
                index={index}
                onEdit={this.edit.bind(this, point)}
                editing={this.state.editing == point}
                onCancel={this.cancel}
                onSave={this.save.bind(this, index)}
              />)
          }
        </Map>
      </div >
    );
  }
}

render(<App />, document.getElementById('root'));

if (module.hot) {
  module.hot.accept();
}